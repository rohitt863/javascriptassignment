const products = [{
    id: 123,
    category: 'Newspaper',
    country: "India",
    tax: 18,
    price: 250,
    quantity: 3,
}, {
    id: 124,
    category: 'Newspaper',
    country: "Dubai",
    tax: 5,
    price: 300,
    quantity: 7,
}, {
    id: 125,
    category: 'Newspaper',
    country: "London",
    tax: 22,
    price: 400,
    quantity: 10,
}, {
    id: 126,
    category: 'Newspaper',
    country: "France",
    tax: 13,
    price: 150,
    quantity: 24,
},{
    id: 127,
    category: 'Newspaper',
    country: "France",
    tax: 15,
    price: 350,
    quantity: 4,
}
];


let table = "<table><tr><th></th>";
products.forEach((item)=>{
    table += `<th>${item.country}</th>`;
});

table+="</tr>";

function getFinalCostOfProducts() {
    let finalCostArray = products.map((item)=>{
        return calculateFinalCost(item.price,item.quantity,item.tax);
    });
    return finalCostArray;
}

function calculateFinalCost(price,quantity,tax) {
    return price*quantity+price*quantity*tax/100;
}

let finalCostArray = getFinalCostOfProducts();

table+="<tr><th>category</th>";
products.forEach((item)=>{
    table += `<td>${item.category}</td>`;
});
table+="</tr>";

table+="<tr><th>tax</th>";
products.forEach((item)=>{
    table += `<td>${item.tax}</td>`;
});
table+="</tr>";

table+="<tr><th>price</th>";
products.forEach((item)=>{
    table += `<td>${item.price}</td>`;
});
table+="</tr>";

table+="<tr><th>quantity</th>";
products.forEach((item)=>{
    table += `<td>${item.quantity}</td>`;
});

table+="<tr><th>Final Price</th>";
finalCostArray.forEach((item)=>{
    table += `<td>${item}</td>`;
});



table+="</tr></table>";


console.log(table);
//let tableElement = document.createElement(table);
$("#main").append(table);

function showTax(){
    let tax=$('#tax').val();
    let qty=$('#qty').val();
    let price=$('#price').val();
    $('#finalCost').html(calculateFinalCost(price,qty,tax));
}